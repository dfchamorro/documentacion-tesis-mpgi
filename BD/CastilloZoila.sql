-- CASTILLO ZOILA --
-- 4,58029262 --

INSERT INTO cdla.persona (rut_pasaporte, nombre_civil, apellido_paterno, apellido_materno, fecha_nacimiento, edad, genero, sexo_biologico)
VALUES(58029262, 'ZOILA ROSA', 'CASTILLO', 'MORA', '1939/10/20', 77, 'F', 'F');

INSERT INTO cdla.paciente (cf_persona_paciente, cf_rut_pasaporte_paciente)
VALUES (4,58029262);

-- TOMA DE MUESTRA --
-- MARZO 2018 CASTILLO ZOILA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,4,58029262,'2018/03/01',1,36.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,4,58029262,'2018/03/01',1,12.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,4,58029262,'2018/03/01',1,40);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,4,58029262,'2018/03/01',1,9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,4,58029262,'2018/03/01',1,1.68);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,4,58029262,'2018/03/01',1,1.42);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,4,58029262,'2018/03/01',1,4.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,4,58029262,'2018/03/01',1,8.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,4,58029262,'2018/03/01',1,2.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,4,58029262,'2018/03/01',1,26);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,4,58029262,'2018/03/01',1,15);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,4,58029262,'2018/03/01',1,6.39);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (12,4,58029262,'2018/03/01',1,25);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (15,4,58029262,'2018/03/01',1,293);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (16,4,58029262,'2018/03/01',1,4.20);

-- TOMA DE MUESTRA --
-- ABRIL 2018 CASTILLO ZOILA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,4,58029262,'2018/04/01',1,33.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,4,58029262,'2018/04/01',1,11.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,4,58029262,'2018/04/01',1,39);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,4,58029262,'2018/04/01',1,9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,4,58029262,'2018/04/01',1,1.68);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,4,58029262,'2018/04/01',1,1.42);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,4,58029262,'2018/04/01',1,3.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,4,58029262,'2018/04/01',1,8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,4,58029262,'2018/04/01',1,1.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,4,58029262,'2018/04/01',1,31);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,4,58029262,'2018/04/01',1,18);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,4,58029262,'2018/04/01',1,6.25);

-- TOMA DE MUESTRA --
-- MAYO 2018 CASTILLO ZOILA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,4,58029262,'2018/05/01',1,33.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,4,58029262,'2018/05/01',1,11.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,4,58029262,'2018/05/01',1,38);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,4,58029262,'2018/05/01',1,9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,4,58029262,'2018/05/01',1,1.67);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,4,58029262,'2018/05/01',1,1.41);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,4,58029262,'2018/05/01',1,3.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,4,58029262,'2018/05/01',1,8.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,4,58029262,'2018/05/01',1,2.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,4,58029262,'2018/05/01',1,25);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,4,58029262,'2018/05/01',1,17);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,4,58029262,'2018/05/01',1,6.18);

-- TOMA DE MUESTRA --
-- JUNIO 2018 CASTILLO ZOILA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,4,58029262,'2018/06/01',1,30.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,4,58029262,'2018/06/01',1,10.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,4,58029262,'2018/06/01',1,44);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,4,58029262,'2018/06/01',1,12);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,4,58029262,'2018/06/01',1,1.53);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,4,58029262,'2018/06/01',1,1.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,4,58029262,'2018/06/01',1,3.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,4,58029262,'2018/06/01',1,8.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,4,58029262,'2018/06/01',1,2.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,4,58029262,'2018/06/01',1,18);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,4,58029262,'2018/06/01',1,11);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,4,58029262,'2018/06/01',1,6.42);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (12,4,58029262,'2018/06/01',1,23);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (15,4,58029262,'2018/06/01',1,134);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (16,4,58029262,'2018/06/01',1,4.35);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (17,4,58029262,'2018/06/01',1,230);

-- TOMA DE MUESTRA --
-- JULIO 2018 CASTILLO ZOILA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,4,58029262,'2018/07/01',1,29.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,4,58029262,'2018/07/01',1,10.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,4,58029262,'2018/07/01',1,47);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,4,58029262,'2018/07/01',1,8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,4,58029262,'2018/07/01',1,2.07);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,4,58029262,'2018/07/01',1,1.75);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,4,58029262,'2018/07/01',1,3.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,4,58029262,'2018/07/01',1,8.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,4,58029262,'2018/07/01',1,2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,4,58029262,'2018/07/01',1,24);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,4,58029262,'2018/07/01',1,16);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,4,58029262,'2018/07/01',1,6.49);

-- TOMA DE MUESTRA --
-- AGOSTO 2018 CASTILLO ZOILA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,4,58029262,'2018/08/01',1,28);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,4,58029262,'2018/08/01',1,9.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,4,58029262,'2018/08/01',1,25);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,4,58029262,'2018/08/01',1,6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,4,58029262,'2018/08/01',1,1.67);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,4,58029262,'2018/08/01',1,1.41);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,4,58029262,'2018/08/01',1,3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,4,58029262,'2018/08/01',1,8.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,4,58029262,'2018/08/01',1,1.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,4,58029262,'2018/08/01',1,34);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,4,58029262,'2018/08/01',1,17);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,4,58029262,'2018/08/01',1,6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (18,4,58029262,'2018/08/01',1,831);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (19,4,58029262,'2018/08/01',1,57);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (20,4,58029262,'2018/08/01',1,177);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (21,4,58029262,'2018/08/01',1,24);