-- GAMORAL MARIANA --
-- 6,75282699 --

INSERT INTO cdla.persona (rut_pasaporte, nombre_civil, apellido_paterno, apellido_materno, fecha_nacimiento, edad, genero, sexo_biologico)
VALUES(75282699, 'MARIANA', 'DIAZ', 'GAMONAL', '1942/04/10', 77, 'F', 'F');

INSERT INTO cdla.paciente (cf_persona_paciente, cf_rut_pasaporte_paciente)
VALUES (6,75282699);

-- TOMA DE MUESTRA --
-- NOVIEMBRE 2019 GAMORAL MARIANA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,6,75282699,'2019/11/01',1,10.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,6,75282699,'2019/11/01',1,68);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,6,75282699,'2019/11/01',1,14.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,6,75282699,'2019/11/01',1,1.73);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,6,75282699,'2019/11/01',1,1.46);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,6,75282699,'2019/11/01',1,3.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,6,75282699,'2019/11/01',1,8.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,6,75282699,'2019/11/01',1,3.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,6,75282699,'2019/11/01',1,13);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,6,75282699,'2019/11/01',1,7);

-- TOMA DE MUESTRA --
-- DICIEMBRE 2019 GAMORAL MARIANA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,6,75282699,'2019/12/01',1,28.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,6,75282699,'2019/12/01',1,9.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,6,75282699,'2019/12/01',1,71.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,6,75282699,'2019/12/01',1,19.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,6,75282699,'2019/12/01',1,1.52);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,6,75282699,'2019/12/01',1,1.27);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,6,75282699,'2019/12/01',1,4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,6,75282699,'2019/12/01',1,8.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,6,75282699,'2019/12/01',1,4.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,6,75282699,'2019/12/01',1,16);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,6,75282699,'2019/12/01',1,12);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,6,75282699,'2019/12/01',1,6.35);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (12,6,75282699,'2019/12/01',1,26.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (15,6,75282699,'2019/12/01',1,175);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (16,6,75282699,'2019/12/01',1,3.85);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (17,6,75282699,'2019/12/01',1,410);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (18,6,75282699,'2019/12/01',1,531);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (19,6,75282699,'2019/12/01',1,75);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (20,6,75282699,'2019/12/01',1,119);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (21,6,75282699,'2019/12/01',1,39);

-- TOMA DE MUESTRA --
-- ENERO 2020 GAMORAL MARIANA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,6,75282699,'2020/01/01',1,10.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,6,75282699,'2020/01/01',1,44.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,6,75282699,'2020/01/01',1,8.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,6,75282699,'2020/01/01',1,1.88);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,6,75282699,'2020/01/01',1,1.59);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,6,75282699,'2020/01/01',1,3.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,6,75282699,'2020/01/01',1,8.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,6,75282699,'2020/01/01',1,3.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,6,75282699,'2020/01/01',1,13);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,6,75282699,'2020/01/01',1,7);

-- TOMA DE MUESTRA --
-- FEBRERO 2020 GAMORAL MARIANA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,6,75282699,'2020/02/01',1,26.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,6,75282699,'2020/02/01',1,8.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,6,75282699,'2020/02/01',1,38.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,6,75282699,'2020/02/01',1,6.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,6,75282699,'2020/02/01',1,2.01);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,6,75282699,'2020/02/01',1,1.70);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,6,75282699,'2020/02/01',1,3.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,6,75282699,'2020/02/01',1,8.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,6,75282699,'2020/02/01',1,4.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,6,75282699,'2020/02/01',1,16);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,6,75282699,'2020/02/01',1,8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,6,75282699,'2020/02/01',1,5.89);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (17,6,75282699,'2020/02/01',1,500);

-- TOMA DE MUESTRA --
-- MARZO 2020 GAMORAL MARIANA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,6,75282699,'2020/03/01',1,29.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,6,75282699,'2020/03/01',1,9.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,6,75282699,'2020/03/01',1,35.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,6,75282699,'2020/03/01',1,7.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,6,75282699,'2020/03/01',1,1.72);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,6,75282699,'2020/03/01',1,1.46);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,6,75282699,'2020/03/01',1,3.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,6,75282699,'2020/03/01',1,8.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,6,75282699,'2020/03/01',1,2.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,6,75282699,'2020/03/01',1,13);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,6,75282699,'2020/03/01',1,9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,6,75282699,'2020/03/01',1,4.51);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (12,6,75282699,'2020/03/01',1,25);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (15,6,75282699,'2020/03/01',1,150);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (16,6,75282699,'2020/03/01',1,3.26);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (11,6,75282699,'2020/03/01',1,182);

-- TOMA DE MUESTRA --
-- ABRIL 2020 GAMORAL MARIANA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,6,75282699,'2020/04/01',1,8.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,6,75282699,'2020/04/01',1,42.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,6,75282699,'2020/04/01',1,11.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,6,75282699,'2020/04/01',1,1.47);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,6,75282699,'2020/04/01',1,1.21);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,6,75282699,'2020/04/01',1,3.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,6,75282699,'2020/04/01',1,7.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,6,75282699,'2020/04/01',1,2.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,6,75282699,'2020/04/01',1,9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,6,75282699,'2020/04/01',1,7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (11,6,75282699,'2020/04/01',1,220);