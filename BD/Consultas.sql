-- BERNARDITA ACUÑA --
-- 1,75684894 --

INSERT INTO cdla.persona (rut_pasaporte, nombre_civil, apellido_paterno, apellido_materno, fecha_nacimiento, edad, genero, sexo_biologico)
VALUES(75684894, 'BERNARDITA', 'ACUÑA', 'SANHUEZA', '1953/02/20', 66, 'F', 'F');

INSERT INTO cdla.paciente (cf_persona_paciente, cf_rut_pasaporte_paciente)
VALUES (1,75684894);

-- EXAMEN --

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('Hematocrito','Mensual',47,37);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2, valor_ref_3)
VALUES ('Hemoglobina','Mensual',16,12,10);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2,  valor_ref_3, valor_ref_4)
VALUES ('N.U.Pre','Mensual',120,100,23,9);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('N.U.Post','Mensual',23,9);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2, valor_ref_3, valor_ref_4)
VALUES ('Potasio','Mensual',6.5,6.0,5.5,3.5);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('Calcio','Mensual',10.4,8.7);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('Fósforo','Mensual',5.1,2.4);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('GOT','Mensual',40,2);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('GPT','Mensual',40,2);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('Creatinina','Mensual',1.1,0.5);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('Glicemia','Trimestral',106,74);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('Bicarbonato','Semestral',106,74);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('KtV','Mensual',1.4,1.2);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1)
VALUES ('eqKtV','Mensual',1.2);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('F.Alcalinas','Mensual',290,70);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('Albúminas','Mensual',4.8,3.2);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('PTH-I','Mensual',78,11);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2, valor_ref_3)
VALUES ('Ferritina','Semestral',500,233,14);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('Ferremia','Semestral',160,60);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('T.B.I.C','Semestral',400,250);

INSERT INTO cdla.examen (nombre, periodicidad, valor_ref_1, valor_ref_2)
VALUES ('Porcentaje de Saturación','Semestral',55,20);

-- LABORATORIO --

INSERT INTO cdla.laboratorio (idlaboratorio,nombre,direccion)
VALUES (1,'Laboratorio Román Días','Rengo');

-- TOMA DE MUESTRA --
-- NOVIEMBRE 2019 BERNARDITA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,1,75684894,'2019/11/01',1,10.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,1,75684894,'2019/11/01',1,30.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,1,75684894,'2019/11/01',1,6.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,1,75684894,'2019/11/01',1,1.81);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,1,75684894,'2019/11/01',1,1.53);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,1,75684894,'2019/11/01',1,3.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,1,75684894,'2019/11/01',1,9.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,1,75684894,'2019/11/01',1,3.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,1,75684894,'2019/11/01',1,37);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,1,75684894,'2019/11/01',1,16);

-- TOMA DE MUESTRA --
-- DICIEMBRE 2019 BERNARDITA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,1,75684894,'2019/12/01',1,35);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,1,75684894,'2019/12/01',1,10.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,1,75684894,'2019/12/01',1,29.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,1,75684894,'2019/12/01',1,5.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,1,75684894,'2019/12/01',1,1.87);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,1,75684894,'2019/12/01',1,1.56);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,1,75684894,'2019/12/01',1,4.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,1,75684894,'2019/12/01',1,9.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,1,75684894,'2019/12/01',1,4.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,1,75684894,'2019/12/01',1,39);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,1,75684894,'2019/12/01',1,18);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,1,75684894,'2019/12/01',1,7.77);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (12,1,75684894,'2019/12/01',1,25);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (15,1,75684894,'2019/12/01',1,481);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (16,1,75684894,'2019/12/01',1,3.42);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (17,1,75684894,'2019/12/01',1,2405);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (18,1,75684894,'2019/12/01',1,366);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (19,1,75684894,'2019/12/01',1,33);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (20,1,75684894,'2019/12/01',1,128);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (21,1,75684894,'2019/12/01',1,20);

-- TOMA DE MUESTRA --
-- FEBRERO 2020 BERNARDITA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,1,75684894,'2020/02/01',1,30.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,1,75684894,'2020/02/01',1,9.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,1,75684894,'2020/02/01',1,18.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,1,75684894,'2020/02/01',1,3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,1,75684894,'2020/02/01',1,2.05);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,1,75684894,'2020/02/01',1,1.73);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,1,75684894,'2020/02/01',1,3.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,1,75684894,'2020/02/01',1,8.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,1,75684894,'2020/02/01',1,3.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,1,75684894,'2020/02/01',1,25);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,1,75684894,'2020/02/01',1,12);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,1,75684894,'2020/02/01',1,6.86);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (17,1,75684894,'2020/02/01',1,1760);

-- TOMA DE MUESTRA --
-- MARZO 2020 BERNARDITA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,1,75684894,'2020/03/01',1,32.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,1,75684894,'2020/03/01',1,10.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,1,75684894,'2020/03/01',1,15.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,1,75684894,'2020/03/01',1,2.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,1,75684894,'2020/03/01',1,1.93);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,1,75684894,'2020/03/01',1,1.63);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,1,75684894,'2020/03/01',1,3.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,1,75684894,'2020/03/01',1,8.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,1,75684894,'2020/03/01',1,3.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,1,75684894,'2020/03/01',1,31);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,1,75684894,'2020/03/01',1,14);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,1,75684894,'2020/03/01',1,5.88);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (12,1,75684894,'2020/03/01',1,26.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (15,1,75684894,'2020/03/01',1,296);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (16,1,75684894,'2020/03/01',1,2.59);

-- TOMA DE MUESTRA --
-- ABRIL 2020 BERNARDITA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,1,75684894,'2020/04/01',1,10.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,1,75684894,'2020/04/01',1,30.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,1,75684894,'2020/04/01',1,7.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,1,75684894,'2020/04/01',1,1.59);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,1,75684894,'2020/04/01',1,1.35);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,1,75684894,'2020/04/01',1,3.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,1,75684894,'2020/04/01',1,9.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,1,75684894,'2020/04/01',1,2.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,1,75684894,'2020/04/01',1,48);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,1,75684894,'2020/04/01',1,25);

-- TOMA DE MUESTRA --
-- MAYO 2020 BERNARDITA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,1,75684894,'2020/05/01',1,9.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,1,75684894,'2020/05/01',1,61.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,1,75684894,'2020/05/01',1,39.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,1,75684894,'2020/05/01',1,0.51);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,1,75684894,'2020/05/01',1,0.44);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,1,75684894,'2020/05/01',1,4.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,1,75684894,'2020/05/01',1,9.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,1,75684894,'2020/05/01',1,5.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,1,75684894,'2020/05/01',1,22);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,1,75684894,'2020/05/01',1,13);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,1,75684894,'2020/05/01',1,10.15);

-- HERRAMIENTAS --

update cdla.examen
set valor_referencia_max = 16, valor_referencia_minimo = 12
where idexamen = 2;

select * from cdla.toma_muestra;

CREATE TABLE IF NOT EXISTS `cdla`.`toma_muestra` (
  `cf_persona_paciente` int(10) unsigned NOT NULL,
  `cf_rut_pasaporte_paciente` int(10) unsigned NOT NULL,
  `cf_idexamen` int(10) unsigned NOT NULL,
  `resultado` float DEFAULT NULL,
  `cf_idlaboratorio` int(10) unsigned NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`cf_persona_paciente`,`cf_rut_pasaporte_paciente`,`cf_idexamen`,`cf_idlaboratorio`,`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8