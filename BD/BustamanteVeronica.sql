-- BUSTAMANTE VERONICA --
-- 3,102479734 --

INSERT INTO cdla.persona (rut_pasaporte, nombre_civil, apellido_paterno, apellido_materno, fecha_nacimiento, edad, genero, sexo_biologico)
VALUES(102479734, 'VERONICA', 'BUSTAMANTE', 'VALENZUELA', '1965/09/03', 53, 'F', 'F');

INSERT INTO cdla.paciente (cf_persona_paciente, cf_rut_pasaporte_paciente)
VALUES (3,102479734);

-- TOMA DE MUESTRA --
-- AGOSTO 2018 BUSTAMANTE VERONICA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,3,102479734,'2018/08/01',1,31.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,3,102479734,'2018/08/01',1,11);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,3,102479734,'2018/08/01',1,64);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,3,102479734,'2018/08/01',1,24);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,3,102479734,'2018/08/01',1,1.17);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,3,102479734,'2018/08/01',1,1.02);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,3,102479734,'2018/08/01',1,4.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,3,102479734,'2018/08/01',1,9.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,3,102479734,'2018/08/01',1,5.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,3,102479734,'2018/08/01',1,15);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,3,102479734,'2018/08/01',1,20);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,3,102479734,'2018/08/01',1,8.08);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (11,3,102479734,'2018/08/01',1,94);

-- TOMA DE MUESTRA --
-- SEPTIEMBRE 2018 BUSTAMANTE VERONICA--

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,3,102479734,'2018/09/01',1,32.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,3,102479734,'2018/09/01',1,10.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,3,102479734,'2018/09/01',1,63);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,3,102479734,'2018/09/01',1,20);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,3,102479734,'2018/09/01',1,1.41);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,3,102479734,'2018/09/01',1,1.23);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,3,102479734,'2018/09/01',1,4.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,3,102479734,'2018/09/01',1,9.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,3,102479734,'2018/09/01',1,7.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,3,102479734,'2018/09/01',1,14);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,3,102479734,'2018/09/01',1,17);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,3,102479734,'2018/09/01',1,7.79);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (12,3,102479734,'2018/09/01',1,25);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (11,3,102479734,'2018/09/01',1,454);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (15,3,102479734,'2018/09/01',1,665);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (16,3,102479734,'2018/09/01',1,4.28);

-- TOMA DE MUESTRA --
-- OCTUBRE 2018 BUSTAMANTE VERONICA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,3,102479734,'2018/10/01',1,31.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,3,102479734,'2018/10/01',1,10);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,3,102479734,'2018/10/01',1,53);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,3,102479734,'2018/10/01',1,15);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,3,102479734,'2018/10/01',1,1.47);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,3,102479734,'2018/10/01',1,1.28);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,3,102479734,'2018/10/01',1,5.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,3,102479734,'2018/10/01',1,9.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,3,102479734,'2018/10/01',1,5.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,3,102479734,'2018/10/01',1,17);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,3,102479734,'2018/10/01',1,17);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,3,102479734,'2018/10/01',1,7.78);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (11,3,102479734,'2018/10/01',1,294);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (17,3,102479734,'2018/10/01',1,965);

-- TOMA DE MUESTRA --
-- NOVIEMBRE 2018 BUSTAMANTE VERONICA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,3,102479734,'2018/11/01',1,33.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,3,102479734,'2018/11/01',1,11);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,3,102479734,'2018/11/01',1,65.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,3,102479734,'2018/11/01',1,18.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,3,102479734,'2018/11/01',1,1.52);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,3,102479734,'2018/11/01',1,1.32);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,3,102479734,'2018/11/01',1,3.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,3,102479734,'2018/11/01',1,9.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,3,102479734,'2018/11/01',1,5.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,3,102479734,'2018/11/01',1,16);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,3,102479734,'2018/11/01',1,14);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,3,102479734,'2018/11/01',1,6.51);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (11,3,102479734,'2018/11/01',1,190);

-- TOMA DE MUESTRA --
-- DICIEMBRE 2018 BUSTAMANTE VERONICA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,3,102479734,'2018/12/01',1,30.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,3,102479734,'2018/12/01',1,9.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,3,102479734,'2018/12/01',1,43.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,3,102479734,'2018/12/01',1,15);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,3,102479734,'2018/12/01',1,1.27);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,3,102479734,'2018/12/01',1,1.11);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,3,102479734,'2018/12/01',1,3.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,3,102479734,'2018/12/01',1,9.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,3,102479734,'2018/12/01',1,5.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,3,102479734,'2018/12/01',1,25);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,3,102479734,'2018/12/01',1,24);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,3,102479734,'2018/12/01',1,7.26);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (12,3,102479734,'2018/12/01',1,23.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (11,3,102479734,'2018/12/01',1,270);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (15,3,102479734,'2018/12/01',1,697);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (16,3,102479734,'2018/12/01',1,3.83);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (18,3,102479734,'2018/12/01',1,526);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (19,3,102479734,'2018/12/01',1,20);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (20,3,102479734,'2018/12/01',1,127);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (21,3,102479734,'2018/12/01',1,14);

-- TOMA DE MUESTRA --
-- ENERO 2019 BUSTAMANTE VERONICA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,3,102479734,'2019/01/01',1,36.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,3,102479734,'2019/01/01',1,11.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,3,102479734,'2019/01/01',1,48.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,3,102479734,'2019/01/01',1,10.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,3,102479734,'2019/01/01',1,1.83);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,3,102479734,'2019/01/01',1,1.59);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,3,102479734,'2019/01/01',1,4.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,3,102479734,'2019/01/01',1,9.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,3,102479734,'2019/01/01',1,4.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,3,102479734,'2019/01/01',1,13);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,3,102479734,'2019/01/01',1,15);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,3,102479734,'2019/01/01',1,5.14);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (11,3,102479734,'2019/01/01',1,78);