-- JOSE AYALA --
-- 2,93153847 --

INSERT INTO cdla.persona (rut_pasaporte, nombre_civil, apellido_paterno, apellido_materno, fecha_nacimiento, edad, genero, sexo_biologico)
VALUES(93153847, 'JOSE', 'AYALA', 'INOSTROZA', '1962/07/10', 56, 'M', 'M');

INSERT INTO cdla.paciente (cf_persona_paciente, cf_rut_pasaporte_paciente)
VALUES (2,93153847);

-- TOMA DE MUESTRA --
-- SEPTIEMBRE 2018 JOSE AYALA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,2,93153847,'2018/09/01',1,20.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,2,93153847,'2018/09/01',1,6.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,2,93153847,'2018/09/01',1,30);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,2,93153847,'2018/09/01',1,9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,2,93153847,'2018/09/01',1,1.34);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,2,93153847,'2018/09/01',1,1.17);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,2,93153847,'2018/09/01',1,4.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,2,93153847,'2018/09/01',1,8.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,2,93153847,'2018/09/01',1,4.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,2,93153847,'2018/09/01',1,12);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,2,93153847,'2018/09/01',1,7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,2,93153847,'2018/09/01',1,12.04);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (12,2,93153847,'2018/09/01',1,27);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (15,2,93153847,'2018/09/01',1,105);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (16,2,93153847,'2018/09/01',1,3.46);

-- TOMA DE MUESTRA --
-- OCTUBRE 2018 JOSE AYALA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,2,93153847,'2018/10/01',1,22.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,2,93153847,'2018/10/01',1,7.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,2,93153847,'2018/10/01',1,53);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,2,93153847,'2018/10/01',1,21);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,2,93153847,'2018/10/01',1,1.09);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,2,93153847,'2018/10/01',1,0.96);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,2,93153847,'2018/10/01',1,3.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,2,93153847,'2018/10/01',1,8.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,2,93153847,'2018/10/01',1,4.5);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,2,93153847,'2018/10/01',1,7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,2,93153847,'2018/10/01',1,7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,2,93153847,'2018/10/01',1,12.46);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (20,2,93153847,'2018/10/01',1,578);

-- TOMA DE MUESTRA --
-- NOVIEMBRE 2018 JOSE AYALA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,2,93153847,'2018/11/01',1,24.7);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,2,93153847,'2018/11/01',1,8.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,2,93153847,'2018/11/01',1,65.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,2,93153847,'2018/11/01',1,23);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,2,93153847,'2018/11/01',1,1.22);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,2,93153847,'2018/11/01',1,1.07);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,2,93153847,'2018/11/01',1,3.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,2,93153847,'2018/11/01',1,9.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,2,93153847,'2018/11/01',1,5.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,2,93153847,'2018/11/01',1,232);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,2,93153847,'2018/11/01',1,796);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,2,93153847,'2018/11/01',1,11.83);

-- TOMA DE MUESTRA --
-- DICIEMBRE 2018 JOSE AYALA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,2,93153847,'2018/12/01',1,23.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,2,93153847,'2018/12/01',1,7.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,2,93153847,'2018/12/01',1,59.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,2,93153847,'2018/12/01',1,21.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,2,93153847,'2018/12/01',1,1.16);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,2,93153847,'2018/12/01',1,1.02);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,2,93153847,'2018/12/01',1,4.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,2,93153847,'2018/12/01',1,9.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,2,93153847,'2018/12/01',1,6.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,2,93153847,'2018/12/01',1,9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,2,93153847,'2018/12/01',1,10);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,2,93153847,'2018/12/01',1,11.59);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (12,2,93153847,'2018/12/01',1,22.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (15,2,93153847,'2018/12/01',1,186);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (16,2,93153847,'2018/12/01',1,3.57);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (18,2,93153847,'2018/12/01',1,311);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (19,2,93153847,'2018/12/01',1,28);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (20,2,93153847,'2018/12/01',1,239);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (21,2,93153847,'2018/12/01',1,10);

-- TOMA DE MUESTRA --
-- ENERO 2019 JOSE AYALA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (1,2,93153847,'2019/01/01',1,21.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,2,93153847,'2019/01/01',1,6.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,2,93153847,'2019/01/01',1,59.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,2,93153847,'2019/01/01',1,19.4);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,2,93153847,'2019/01/01',1,1.38);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,2,93153847,'2019/01/01',1,1.20);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,2,93153847,'2019/01/01',1,5.6);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,2,93153847,'2019/01/01',1,9.8);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,2,93153847,'2019/01/01',1,5.1);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,2,93153847,'2019/01/01',1,9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,2,93153847,'2019/01/01',1,11);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (10,2,93153847,'2019/01/01',1,9.20);

-- TOMA DE MUESTRA --
-- FEBRERO 2019 JOSE AYALA --

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (2,2,93153847,'2019/02/01',1,7.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (3,2,93153847,'2019/02/01',1,70.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (4,2,93153847,'2019/02/01',1,25.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (13,2,93153847,'2019/02/01',1,1.20);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (14,2,93153847,'2019/02/01',1,1.05);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (5,2,93153847,'2019/02/01',1,5.3);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (6,2,93153847,'2019/02/01',1,8.9);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (7,2,93153847,'2019/02/01',1,5.2);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (8,2,93153847,'2019/02/01',1,23);

INSERT INTO cdla.toma_muestra (cf_idexamen,cf_persona_paciente,cf_rut_pasaporte_paciente,fecha,cf_idlaboratorio,resultado)
VALUES (9,2,93153847,'2019/02/01',1,82);

Select *
from cdla.toma_muestra
where fecha = '2019/02/01';
